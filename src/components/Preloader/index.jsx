import React from 'react';
import { Dimmer, Loader } from 'semantic-ui-react';

const Preloader = () => (
  <Dimmer active inverted>
    <Loader size="massive" inverted className="preloader" />
  </Dimmer>
);

export default Preloader;
