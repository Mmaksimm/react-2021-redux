import React, { useState } from 'react'
import PropTypes from 'prop-types';
import { Button, Icon, Modal, Form } from 'semantic-ui-react'

import styles from './styles.module.scss';

const EditMessage = ({
  updateMessage,
  message,
  cancel,
  editModal
}) => {
  const [getText, setText] = useState(message.text)
  const HandlerOnChange = ({ target: { value } }) => {
    setText(value);
  }

  const handlerUpdate = () => {
    const text = getText.trim();
    if (!text || getText === message.text) return;
    updateMessage({ text: getText, messageId: message.id })
  }

  return (
    <Modal open={editModal} className={`edit-message-modal ${editModal ? 'modal-shown' : ''}`}>
      <Modal.Header className={styles.modalHeader}>Edit your message</Modal.Header>
      <Modal.Content className={styles.modalContent}>
        <Modal.Description>
          <Form >
            <div>
              <
                textarea
                name="body"
                placeholder="What is the news?"
                rows="3"
                className="edit-message-input"
                value={getText}
                onChange={HandlerOnChange}
              />
            </div>
          </Form>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions className={styles.modalAction}>
        <Button.Group>
          <Button icon onClick={handlerUpdate} className="edit-message-button">
            <Icon name="save" />
          </Button>
          <Button icon onClick={cancel} className="edit-message-close">
            <Icon name="window close" />
          </Button>
        </Button.Group>
      </Modal.Actions>
    </Modal>
  )
}

EditMessage.propTypes = {
  updateMessage: PropTypes.func.isRequired,
  message: PropTypes.objectOf(
    PropTypes.oneOfType(
      [PropTypes.string, PropTypes.bool]
    )
  ).isRequired,
  cancel: PropTypes.func.isRequired,
  editModal: PropTypes.bool.isRequired
}

export default EditMessage
