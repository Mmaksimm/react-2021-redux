import uuid from 'react-uuid';

import {
  SET_ALL_MESSAGES,
  SET_EDIT_MESSAGE,
  CANCEL
} from './actionTypes';

export const setMessages = messages => ({
  type: SET_ALL_MESSAGES,
  payload: messages
});

export const addNewMessage = text => (dispatch, getRootState) => {
  const { chat: { messages } } = getRootState();
  const { profile: { ownUsername, ownUserId } } = getRootState();

  const newMessage = {
    text,
    id: uuid(),
    userId: ownUserId,
    avatar: "",
    user: ownUsername,
    createdAt: new Date().toISOString(),
    editedAt: "",
    isLike: false
  }

  dispatch(setMessages({ messages: [...messages, newMessage] }))
};

export const likeToggle = ({ isLike, messageId }) => (dispatch, getRootState) => {
  const updatedMessage = message => ({ ...message, isLike })
  const { chat: { messages } } = getRootState();

  const updateMessages = messages.map(message => message.id !== messageId
    ? message
    : updatedMessage(message)
  );

  dispatch(setMessages({ messages: updateMessages }))
};

export const deleteMessage = messageId => (dispatch, getRootState) => {
  const { chat: { messages } } = getRootState();
  const updateMessages = messages.filter(message => (message.id !== messageId));

  dispatch(setMessages({ messages: updateMessages }));
  dispatch(cancel());
};

export const editMessage = message => ({
  type: SET_EDIT_MESSAGE,
  payload: { message }
});

export const cancel = () => ({
  type: CANCEL
});

export const updateMessage = ({ messageId, text }) => (dispatch, getRootState) => {
  const { chat: { messages } } = getRootState();
  const updatedMessage = message => ({
    ...message,
    text,
    editedAt: new Date().toISOString()
  })

  const updateMessages = messages.map(message => message.id !== messageId
    ? message
    : updatedMessage(message)
  );

  dispatch(setMessages({ messages: updateMessages }))
  dispatch(cancel());
};
