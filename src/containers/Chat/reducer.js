/* eslint-disable import/no-anonymous-default-export */
import uuid from 'react-uuid';
import {
  SET_ALL_MESSAGES,
  SET_EDIT_MESSAGE,
  CANCEL
} from './actionTypes';

const initialState = {
  profile: {
    ownUserId: uuid(),
    ownUsername: 'You'
  },
  chat: {
    messages: [],
    editModal: false,
    preloader: true,
    modalEditMessage: {},
  }
};

export default (state = initialState, action) => {
  switch (action.type) {

    case SET_ALL_MESSAGES:
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: action.payload.messages,
          preloader: false
        }
      };

    case SET_EDIT_MESSAGE:
      return {
        ...state,
        chat: {
          ...state.chat,
          editModal: true,
          modalEditMessage: action.payload.message
        }
      };

    case CANCEL:
      return {
        ...state,
        chat: {
          ...state.chat,
          modalEditMessage: '',
          editModal: false
        }
      };

    default:
      return state;
  }
};
