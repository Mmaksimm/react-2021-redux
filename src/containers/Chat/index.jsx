/* eslint-disable no-undef */
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Container } from 'semantic-ui-react';
import getAllMessagesService from '../../services/messageService';
import MessageList from '../MessageList'
import Header from '../../components/Header';
import MessageInput from '../../components/MessageInput';
import EditMessage from '../../components/EditMessage';

import styles from './styles.module.scss';

import {
  setMessages,
  addNewMessage,
  deleteMessage,
  editMessage,
  cancel,
  updateMessage,
  likeToggle
} from './actions';

const Chat = ({
  url,
  messages,
  ownUserId,
  modalEditMessage,
  editModal,
  setMessages: setMessagesAction,
  addNewMessage: addNewMessageAction,
  deleteMessage: deleteMessageAction,
  editMessage: editMessageAction,
  cancel: cancelAction,
  updateMessage: updateMessageAction,
  likeToggle: likeToggleAction
}) => {
  useEffect(() => {
    const getAllMessages = async () => {
      const messages = await getAllMessagesService(url);
      setMessagesAction({ messages });
    }

    if (!messages?.length) getAllMessages();
  });

  return (
    <>
      <Container className={`${styles.chart} chart`}>
        <Header messages={messages} />
        <MessageList
          messages={messages}
          ownUserId={ownUserId}
          deleteMessage={deleteMessageAction}
          editMessage={editMessageAction}
          likeToggle={likeToggleAction}
        />
        <MessageInput addNewMessage={addNewMessageAction} />
      </Container>
      {(editModal) && (
        <EditMessage
          updateMessage={updateMessageAction}
          message={modalEditMessage}
          cancel={cancelAction}
          editModal={editModal}
        />
      )}
    </>
  )
};

Chat.propTypes = {
  url: PropTypes.string.isRequired,
  messages: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType(
        [PropTypes.string, PropTypes.bool]
      )
    )
  ),
  ownUserId: PropTypes.string,
  editMessage: PropTypes.func.isRequired,
  likeToggle: PropTypes.func.isRequired,
  modalEditMessage: PropTypes.oneOfType([
    PropTypes.objectOf(
      PropTypes.oneOfType(
        [PropTypes.string, PropTypes.bool]
      )
    ),
    PropTypes.string.isRequired
  ]),
  editModal: PropTypes.bool.isRequired,
  setMessages: PropTypes.func.isRequired,
  addNewMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired,
  cancel: PropTypes.func.isRequired,
  updateMessage: PropTypes.func.isRequired,
};

const mapStateToProps = rootState => ({
  messages: rootState.chat.messages,
  ownUserId: rootState.profile.ownUserId,
  modalEditMessage: rootState.chat.modalEditMessage,
  preloader: rootState.chat.preloader,
  editMessage: rootState.chat.editMessage,
  editModal: rootState.chat.editModal
});


Chat.defaultProps = {
  messages: []
};

const actions = {
  setMessages,
  addNewMessage,
  likeToggle,
  deleteMessage,
  editMessage,
  cancel,
  updateMessage
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
