// eslint-disable-next-line no-unused-vars
import React from 'react';
import ReactDOM from 'react-dom';
import store from './store';
import { Provider } from 'react-redux';
import Chat from './containers/Chat';

import './styles/index.css'
import 'semantic-ui-css/semantic.min.css';

const url = 'https://edikdolynskyi.github.io/react_sources/messages.json';
const root = document.getElementById('root');

ReactDOM.render(
  <Provider store={store}>
    <Chat url={url} />
  </Provider>
  , root);
