const getStartDate = isoDate => {
  const date = new Date(Date.parse(isoDate));
  return new Date(
    `${date.getFullYear()} ${date.getMonth()} ${date.getDate()}`
  )
};

export default getStartDate;
